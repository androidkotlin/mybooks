package ch.teko.olten.android.mybooks

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import ch.teko.olten.android.mybooks.model.Book

class BookAdapter(val context: Context, val books:List<Book>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(context)
        val bookView = layoutInflater.inflate(R.layout.book_record, parent, false)

        val bookIdTxt = bookView.findViewById<TextView>(R.id.recordBookId)
        val titleTxt = bookView.findViewById<TextView>(R.id.recordBookTitle)
        val borrowedTxt = bookView.findViewById<TextView>(R.id.recordBookBorrowed)

        val book = books.get(position)

        bookIdTxt.text = book.id.toString()
        titleTxt.text = book.title
        borrowedTxt.text = if (book.borrowed) "ja" else "nein"

        return bookView
    }

    override fun getItem(position: Int): Any {
        return books.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return books.size
    }

}