package ch.teko.olten.android.mybooks.model

object BookIdGenerator {

    private var id = 1

    fun generateId() : Int {
        return id++
    }
}