package ch.teko.olten.android.mybooks

object Keys {

    val BOOK_ID = "bookID"

    val BOOK_REQUEST_CODE = 1

}