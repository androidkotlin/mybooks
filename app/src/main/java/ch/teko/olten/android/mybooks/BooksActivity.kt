package ch.teko.olten.android.mybooks

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ch.teko.olten.android.mybooks.model.BooksService
import kotlinx.android.synthetic.main.activity_books.*

class BooksActivity : AppCompatActivity() {

    val bookService = BooksService()
    val bookAdapter = BookAdapter(this, bookService.getAllBooks())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)

        bookListView.adapter = bookAdapter

        bookListView.setOnItemClickListener { parent, view, position, id ->
            val book = bookService.getAllBooks().get(position)
            val navigateToChangeBookActivity = Intent(this, ChangeBookActivity::class.java)
            navigateToChangeBookActivity.putExtra(Keys.BOOK_ID, book.id)
            startActivityForResult(navigateToChangeBookActivity, Keys.BOOK_REQUEST_CODE)
        }

        newBookBtn.setOnClickListener {
            val navigateToNewBookActivity = Intent(this, NewBookActivity::class.java)
            startActivityForResult(navigateToNewBookActivity, Keys.BOOK_REQUEST_CODE)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        bookAdapter.notifyDataSetChanged()
    }

}
