package ch.teko.olten.android.mybooks.model.repository

import ch.teko.olten.android.mybooks.model.Book
import ch.teko.olten.android.mybooks.model.repository.inmemory.InMemoryBookRepository

interface BookRepository {

    fun getAllBooks() : List<Book>
    fun createBook(book: Book)
    fun findBook(id: Int) : Book?
    fun deleteBook(id: Int)
    fun updateBook(changedBook: Book)

    companion object {

        private val repository = InMemoryBookRepository()

        fun getRepository() : BookRepository {
            return repository
        }

    }
}