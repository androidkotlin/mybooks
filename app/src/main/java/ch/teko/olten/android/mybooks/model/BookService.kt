package ch.teko.olten.android.mybooks.model

import ch.teko.olten.android.mybooks.model.repository.BookRepository
import ch.teko.olten.android.mybooks.view.BookView

class BookService(val bookView: BookView) {

    private val repository = BookRepository.getRepository()

    fun createBook(title:String, notes:String) {
        repository.createBook(Book(BookIdGenerator.generateId(), title, notes))
        bookView.modelChanged()
    }

    fun findBook(id: Int): Book? {
        return repository.findBook(id)
    }

    fun changeBook(id: Int, title: String, notes: String, borrowed: Boolean) {
        val changedBook = Book(id, title, notes, borrowed)
        repository.updateBook(changedBook)
        bookView.modelChanged()
    }

    fun deleteBook(id: Int) {
        repository.deleteBook(id)
        bookView.modelChanged()
    }
}