package ch.teko.olten.android.mybooks.model.repository.inmemory

import ch.teko.olten.android.mybooks.model.Book
import ch.teko.olten.android.mybooks.model.repository.BookRepository

class InMemoryBookRepository : BookRepository {

    val books = arrayListOf<Book>()

    override fun getAllBooks(): List<Book> {
        return books
    }

    override fun createBook(book: Book) {
        books.add(book)
    }

    override fun findBook(id: Int): Book? {
        return books.find {book ->
            book.id == id
        }
    }

    override fun updateBook(book: Book) {
        val dbBook = findBook(book.id)
        if (dbBook != null) {
            deleteBook(book.id)
            books.add(book)
        }
    }

    override fun deleteBook(id: Int) {
        books.removeIf{ book ->
            book.id == id
        }
    }
}