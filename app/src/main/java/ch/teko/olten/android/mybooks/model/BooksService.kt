package ch.teko.olten.android.mybooks.model

import ch.teko.olten.android.mybooks.model.repository.BookRepository

class BooksService {

    private val repository = BookRepository.getRepository()

    fun getAllBooks() : List<Book> {
        return repository.getAllBooks()
    }
}