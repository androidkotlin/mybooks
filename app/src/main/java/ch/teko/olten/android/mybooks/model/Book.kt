package ch.teko.olten.android.mybooks.model

data class Book(val id:Int, val title:String, val notes:String, val borrowed:Boolean = false)