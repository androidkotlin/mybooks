package ch.teko.olten.android.mybooks

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import ch.teko.olten.android.mybooks.model.BookService
import ch.teko.olten.android.mybooks.view.BookView
import kotlinx.android.synthetic.main.activity_new_book.*

class NewBookActivity : AppCompatActivity(), BookView {

    val bookService = BookService(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_book)

        saveBtn.setOnClickListener {
            val title = changeTitleTxt.text.toString()
            val notes = newNotesTxt.text.toString()
            when (title.isEmpty()) {
                true -> Toast.makeText(this, "Sorry, es ist kein Titel definiert!", Toast.LENGTH_SHORT).show()
                false -> bookService.createBook(title, notes)
            }
        }
    }

    override fun modelChanged() {
        setResult(Activity.RESULT_OK)
        finish()
    }
}
