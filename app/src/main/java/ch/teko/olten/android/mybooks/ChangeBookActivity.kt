package ch.teko.olten.android.mybooks

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import ch.teko.olten.android.mybooks.model.BookService
import ch.teko.olten.android.mybooks.view.BookView
import kotlinx.android.synthetic.main.activity_change_book.*


class ChangeBookActivity : AppCompatActivity(), BookView {

    val bookService = BookService(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_book)
        changeIdTxt.keyListener = null
        changeIdTxt.focusable = View.NOT_FOCUSABLE

        val id = intent.getIntExtra(Keys.BOOK_ID, -1)
        val book = bookService.findBook(id)

        if (book != null) {
            changeIdTxt.setText(book.id.toString())
            changeTitleTxt.setText(book.title)
            changeNotesTxt.setText(book.notes)

            when (book.borrowed) {
                true -> borrowedGroup.check(R.id.borrowedYes)
                false -> borrowedGroup.check(R.id.borrowedNo)
            }
        }

        changeBtn.setOnClickListener {
            when (changeTitleTxt.text.toString().isEmpty()) {
                true -> Toast.makeText(this, "Sorry, es ist kein Titel definiert!", Toast.LENGTH_SHORT).show()
                false -> changeBook(id)
            }
        }

        deleteBtn.setOnClickListener {
            bookService.deleteBook(id)
        }
    }

    private fun changeBook(id: Int) {
        val title = changeTitleTxt.text.toString()
        val notes = changeNotesTxt.text.toString()
        val borrowedRadioBtn = findViewById<RadioButton>(borrowedGroup.checkedRadioButtonId)

        val borrowed: Boolean
        when (borrowedRadioBtn.id) {
            R.id.borrowedYes -> bookService.changeBook(id, title, notes, true)
            R.id.borrowedNo -> bookService.changeBook(id, title, notes, false)
        }
    }

    override fun modelChanged() {
        setResult(Activity.RESULT_OK)
        finish()
    }

}
